// 3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

let getCube= 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);




// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.
const address = ["258 Washington Ave. NW", "California", "90011"];
const [avenue, country, code] = address;
console.log(`I live at ${avenue}, ${country}, ${code}`);




// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
const animal = {
	name: "Lolong",
	type: "Saltwater Crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);




// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(`${number}`));

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const reduceNumber = numbers.reduce((x, y) => x + y,);
console.log(reduceNumber);


	

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const newDog = new Dog(); //or const newDog = new Dog("Frankie", 5, "Miniature Dachshund");

newDog.name = 'Frankie';
newDog.age = 5;
newDog.breed = 'Miniature Dachshund';
console.log(newDog);
